# Praktikum Sistem Multimedia

# No 1
###### https://github.com/DianHasnaRamadhani/UTS-Praktikum-Sistem-Multimedia/tree/main/uts%20image
# No 2
###### Program image yang saya buat adalah kompersi image to sketch, dimana pada saat kita mengupload gambar, maka gambar tersebut yang asalnya berwarna akan berubah menjadi sketsa pensil. Ukuran gambar yang telah diubah menjadi sketsa akan lebih kecil dibanding dengan file asli. Program dibuat menggunakan bahasa pemrograman python. Library python yang digunakan :
###### 1. Flask -> untuk membuat web menggunakan bahasa pemrograman python.
###### 2. OpenCV -> untuk mengolah gambar
###### 3. PIL -> pada progra ini digunakan untuk mengkompres gambar dengan menggunakan teknik ANTIALIAS yang ada dalam library PIL.
###### 4. Tkinter -> agar dapat memilih file melalui jendela pop-up.
###### Algoritma program :
```mermaid
flowchart TD
    A[Input Image] --> B(Image Compression)
    B --> C(Grayscale Conversion)
    C --> D(Gaussian Blur)
    D --> E(Edge Detection)
    E --> F(Invert Colors)
    F --> G(Output Sketch)
```
Berikut demo programnya :
![demo-image](https://gitlab.com/dianhasnaramadhani/praktikum-sistem-multimedia/-/raw/main/demo-image-to-sketch.gif)
###### Link Youtube : https://youtu.be/zBTAAb0ezYQ

# No 3
###### Aspek kecerdasan yang digunakan dalam program ini adalah pengolahan citra. Proses konversi gambar menjadi sketsa mengandalkan pengolahan citra, yaitu teknik untuk memanipulasi citra digital dengan menggunakan algoritma dan metode matematika. Teknik pengolahan citra yang digunakan dalam program image to sketch mungkin meliputi deteksi tepi, segmentasi, dan filterisasi.

# No 4
###### https://github.com/DianHasnaRamadhani/UTS-Praktikum-Sistem-Multimedia/tree/main/uts-audio
# No 5
###### Program audio yang saya buat adalah speech to text, dimana program akan menampilkan suara yang ada pada audio dalam bentuk text. Sebelum diterjemahkan, ukuran file audio akan di kompres. Program ini dibuat menggunakan bahasa pemrograman python. Library python yang digunakan :
###### 1. OS -> untuk mengakses direktori dan membuat direktori baru.
###### 2. Pydub -> untuk mengolah audio. Pada program ini menggunakan modul AudioSegmen untuk mengkompres audio.
###### 3. Flask -> untuk membuat web menggunakan bahasa pemrograman python.
###### 4. Speech Recognition -> untuk melakukan transkripsi (konversi dari suara ke teks) pada data audio.
###### Algoritma program :
```mermaid
flowchart TD
    A[Input Audio] --> B(Audio Compression)
    B --> C(Speech Recognition)
    C --> D{Transcription Successful?}
    D -->|Yes| E[Output Text]
    D -->|No| F[Error Message]
```
###### Berikut demo programnya :
![demo-image](https://gitlab.com/dianhasnaramadhani/praktikum-sistem-multimedia/-/raw/main/demo-speech-to-text.gif)

###### Link Youtube : https://youtu.be/fYdw2PRvTC4
# No 6
###### Aspek kecerdasan yang digunakan dalam program ini adalah Speech Recognition. Proses konversi ucapan menjadi teks menggunakan teknologi speech recognition, yaitu teknik untuk mengubah suara menjadi teks yang dapat dimengerti oleh mesin.
