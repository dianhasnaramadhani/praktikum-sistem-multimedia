# No 1
Disini saya membuat program merge video atau menggabungkan dua atau lebih video, dimana video yang dibisa digabungan hanya yang memiliki rasio 1:1. Saat kita memasukan 2 video atau lebih maka saat disimpan akan menjadi satu video yang isinya dari beberapa video yang telah di upload. Hasil pemrosesan akan diconver terlebih dahulu sehingga mendapatkan ukuran file video yang lebih sedikit. Program ini dibuat menggunakan bahasa pemrograman Python. Berikut library Python yang digunakan :
- Flask -> untuk membangun aplikasi web.
- render_template -> untuk merender template HTML dalam aplikasi Flask.
- request: akses ke permintaan yang dikirim oleh klien ke server Flask, seperti data form dan file yang diunggah.
- redirect: untuk mengarahkan permintaan pengguna ke URL lain dalam aplikasi Flask.
- url_for: untuk menghasilkan URL berdasarkan nama fungsi dan argumen yang diberikan.
- send_file: untuk mengirimkan file sebagai respons ke permintaan klien. Dalam konteks ini, digunakan untuk mengirimkan video hasil penggabungan dan kompresi kepada pengguna.
- VideoFileClip: untuk memanipulasi video, seperti memotong, menggabungkan, dan mengedit video.
- concatenate_videoclips: untuk menggabungkan beberapa video menjadi satu klip video tunggal.
- ffmpeg: untuk memproses video, termasuk kompresi, konversi format, dan operasi pemrosesan video lainnya.
- os: Modul ini menyediakan fungsi-fungsi untuk berinteraksi dengan sistem operasi, seperti mengatur path file dan membuat direktori.

Use Case aplikasi :
1. Penggabungan video untuk presentasi: Program ini dapat digunakan untuk menggabungkan dua video menjadi satu, dengan mengatur tata letak 1:1 agar sesuai dengan tampilan slide presentasi.
2. Pembuatan video Instagram: Dalam platform Instagram, video dengan tata letak 1:1 sangat umum digunakan. Kompresi video juga membantu mengurangi ukuran file agar lebih cepat diunggah.
3. Pembuatan video promosi media sosial: Untuk keperluan pemasaran di media sosial seperti Facebook, Twitter, atau LinkedIn, program ini memungkinkan pengguna untuk menggabungkan beberapa video menjadi satu dengan tata letak 1:1 yang optimal. Dengan mengompres video, ukuran file dapat dikurangi sehingga memudahkan pengguna untuk membagikan video promosi ke berbagai platform media sosial.
4. Pembuatan video tutorial: Program ini juga berguna untuk pembuatan video tutorial yang sesuai dengan format 1:1. Kompresi video akan membantu mengurangi ukuran file agar mudah dibagikan kepada pengguna.
5. Pengeditan video untuk platform berbagi video pendek: Banyak platform berbagi video pendek seperti TikTok atau Snapchat menggunakan tata letak 1:1.
# No 2
```mermaid
flowchart TD
A[Start] --> B[Upload video files]
B --> C[Merge videos]
C --> D[Compress merged video]
D --> E[Download compressed video]
E --> F[End]

style A fill:#86C2A6
style B fill:#86C2A6
style C fill:#86C2A6
style D fill:#86C2A6
style E fill:#86C2A6
style F fill:#86C2A6
```

Berikut adalah penjelasan dari masing-masing langkah dalam flowchart tersebut:

- Start: Tahap awal, dimulai dengan memulai sistem atau alur kerja.

- Upload video files: Langkah ini melibatkan pengguna untuk mengunggah berkas video yang ingin digabungkan dan dikompresi. Pengguna akan memberikan berkas video sebagai input ke dalam sistem.

- Merge videos: Setelah berkas video diunggah, langkah ini akan menggabungkan video-video tersebut menjadi satu video yang utuh. Proses penggabungan dapat melibatkan algoritma atau metode yang digunakan oleh sistem untuk menggabungkan berkas video secara otomatis.

- Compress merged video: Setelah video-video digabungkan, langkah ini akan mengompresi video yang telah digabungkan menjadi ukuran yang lebih kecil. Tujuannya adalah untuk mengurangi ukuran file dan mempertahankan kualitas video sebaik mungkin.

- Download compressed video: Setelah proses kompresi selesai, langkah ini akan memungkinkan pengguna untuk mengunduh video yang telah dikompresi ke perangkat mereka. Pengguna dapat menyimpan video hasil kompresi ini untuk digunakan sesuai kebutuhan.

- End: Tahap akhir, menandakan bahwa alur kerja atau sistem telah selesai.

# No 3
Disini saya membuat aplikasi berbentuk web, berikut link github dari program :
##### https://github.com/DianHasnaRamadhani/Video-Processing.git

# No 4
Link Youtube : https://youtu.be/rZ7W_hWhS30
##### Berikut demo programnya :
##### ![demo-image](https://gitlab.com/dianhasnaramadhani/praktikum-sistem-multimedia/-/raw/main/demo-video.gif)

# No 5
Video processing (pemrosesan video) adalah proses manipulasi, analisis, dan transformasi data video digital untuk tujuan tertentu. Pemrosesan video melibatkan berbagai teknik dan algoritma yang digunakan untuk memodifikasi, meningkatkan, atau mengekstraksi informasi dari video. Konsep pemrosesan video mencakup beberapa aspek seperti filtering, enhancement, deteksi objek, pelacakan objek, dan banyak lagi.

Video compression (kompresi video) adalah proses mengurangi ukuran file video tanpa kehilangan kualitas yang signifikan. Kompresi video penting karena ukuran file video yang besar dapat membutuhkan lebih banyak ruang penyimpanan dan waktu transfer yang lebih lama. Teknik kompresi video bekerja dengan memanfaatkan redundansi dalam data video, baik spasial (antara frame) maupun temporal (dalam frame yang sama). Algoritma kompresi video, seperti MPEG dan H.264/AVC, digunakan untuk mengurangi redundansi dan mengkodekan video dengan cara yang efisien.

Video processing tool development (pengembangan alat pemrosesan video) merujuk pada pembuatan perangkat lunak atau sistem yang dapat digunakan untuk melakukan pemrosesan video. Ini melibatkan pengembangan algoritma, antarmuka pengguna, dan infrastruktur yang mendukung pemrosesan video. Dalam pengembangan alat pemrosesan video, berbagai bahasa pemrograman, perpustakaan komputer vision, dan perangkat lunak pengolahan video seperti OpenCV, FFmpeg, atau Adobe Premiere Pro dapat digunakan.

Pengembangan alat pemrosesan video melibatkan pemahaman yang mendalam tentang konsep pemrosesan video, pemrosesan citra, dan pemrosesan sinyal. Hal ini juga melibatkan pemilihan teknik pemrosesan video yang sesuai, desain antarmuka pengguna yang intuitif, serta optimisasi dan pengujian algoritma pemrosesan video.

Dengan pengembangan alat pemrosesan video, pengguna dapat memanfaatkan berbagai teknik dan fitur pemrosesan video untuk keperluan mereka, seperti editing video, pengolahan video secara real-time, analisis video, dan aplikasi lainnya.

Demikianlah penjelasan mengenai konsep video processing, video compression, dan pengembangan alat pemrosesan video.
