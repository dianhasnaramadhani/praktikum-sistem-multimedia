# No 6
#### Program yang saya buat adalah sistem pakar untuk memprediksi penyakit kulit. Sistem ini menggunakan teknik pemrosesan multimedia dan kecerdasan buatan (Artificial Intelligence) untuk memberikan solusi diagnosis penyakit kulit yang akurat dan cepat. Model pada aplikasi ini dibuat menggunakan Transfer Learning DenseNet21 yang merupakan salah satu arsitektur dari algoritma Convolutional Neural Network (CNN).
#### Program ini termasuk kedalam multimedia processing berbasis Artificial Intelligence denga image sebagai multimedianya. Program berbasis web sehingga memudahkan pengguna dalam menggunakannya, bisa dimana saja dan kapan saja menggunakan program ini.
#### Use case aplikasi :
#### 1. Diagnosis Otomatis: Sistem pakar dapat digunakan untuk mendukung diagnosis otomatis penyakit kulit berdasarkan gejala yang diinputkan oleh pengguna. Sistem akan menganalisis gejala yang diidentifikasi dan memberikan prediksi penyakit kulit yang mungkin dialami oleh pengguna.
#### 2. Konsultasi Jarak Jauh: Sistem pakar dapat digunakan untuk menyediakan konsultasi jarak jauh kepada pengguna yang mengalami masalah kulit. Pengguna dapat mengunggah gambar atau menjawab serangkaian pertanyaan untuk mendapatkan saran atau rekomendasi pengobatan yang tepat.
#### 3. Edukasi dan Informasi: Sistem pakar dapat memberikan informasi dan edukasi kepada pengguna tentang berbagai penyakit kulit, gejala, penyebab, dan pengobatan yang relevan. Pengguna dapat mengakses database informasi yang terkait dengan penyakit kulit untuk meningkatkan pemahaman mereka tentang kondisi kulit mereka.
#### 4. Rekomendasi Produk Perawatan: Sistem pakar dapat memberikan rekomendasi produk perawatan kulit berdasarkan jenis kulit dan masalah yang diidentifikasi oleh pengguna. Ini dapat mencakup rekomendasi produk perawatan, obat-obatan topikal, atau perubahan gaya hidup yang dapat membantu mengatasi masalah kulit.
#### 5. Pemantauan dan Pengelolaan: Sistem pakar dapat digunakan untuk memantau kondisi kulit pengguna secara berkala dan memberikan saran atau langkah-langkah pengelolaan yang diperlukan. Ini dapat melibatkan pengingat untuk menjaga kebersihan kulit, menghindari pemicu tertentu, atau membuat janji dengan dokter kulit
# No 7
#### Program dibuat dalam bentuk aplikasi web. Berikut code program yang dibuat :
#### https://github.com/DianHasnaRamadhani/UAS-Prediksi-Penyakit-Kulit.git
# No 8
#### Berikut URL demo aplikasi :
#### https://youtu.be/v6V9haV08pE
# No 9
Flowchart aplikasi :
```mermaid
flowchart TB
  A[Start] --> B[Unggah Gambar]
  B --> C[Prediksi Penyakit Kulit]
  C --> D{Hasil Prediksi}
  D -- Ya --> E[Tampilkan Jenis Penyakit Kulit]
  D -- Tidak --> F[Tampilkan Pesan Tidak Ditemukan]
  E --> G[Tampilkan Informasi Penyakit]
  G --> H[Akhiri]
  F --> H

```
Penjelasan Flowchart:

- Tahap awal adalah "Start" (A) di mana aliran dimulai.
- Pengguna diminta untuk mengunggah gambar penyakit kulit pada langkah "Unggah Gambar" (B).
- Langkah selanjutnya adalah "Prediksi Penyakit Kulit" (C), di mana sistem pakar menganalisis gambar yang diunggah.
- Setelah itu, sistem memeriksa apakah hasil prediksi tersedia pada langkah "Hasil Prediksi" (D).
- Jika hasil prediksi ditemukan, sistem akan menampilkan jenis penyakit kulit yang diprediksi pada langkah "Tampilkan Jenis Penyakit Kulit" (E).
- Setelah itu, sistem menampilkan informasi terkait penyakit kulit yang diprediksi pada langkah "Tampilkan Informasi Penyakit" (G).
- Pada akhirnya, aliran berakhir di "Akhiri" (H), menandakan bahwa proses selesai.
# No 10
#### Berikut link artiker yang telah dibuat :
#### https://docs.google.com/document/d/1NoW3VhMD39jhKtqql9hw6WGrGrBkA3Bg/edit?usp=sharing&ouid=117491352493784384206&rtpof=true&sd=true
